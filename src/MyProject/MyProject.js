import React from "react";
import "./MyProject.css";
export default function MyProject() {
  return (
    <div className="my-project">
      <p className="text-center text-5xl mt-3 text-danger">My Project</p>
      <div className="flex flex-wrap text-2xl">
        {/* Link project */}
        <div className="basis-1/2 mt-4 pl-20">
          <ol>
            <li>
              <p>1.</p>
              <a href="https://taixiu-game.vercel.app/">
                Game Tài Xỉu (ReactJs)
              </a>
            </li>
            <li>
              <p>2.</p>
              <a href="https://group06-capstone-reactjs.vercel.app/">
                Movie (ReactJs)
              </a>
            </li>
            <li>
              <p>3.</p>
              <a href="https://group06-capstone-airbnb-bc35.vercel.app/">
                Airbnb (ReactJs)
              </a>
            </li>
            <li>
              <p>4.</p>
              <a href="https://trannhathao-bc35-baitap-reactjs-buoi02-state.vercel.app/">
                Glasses (ReactJs)
              </a>
            </li>
            <li>
              <p>5.</p>
              <a href="https://part01-deploy.vercel.app/">
                ShopPhone (JavaScript)
              </a>
            </li>
            <li>
              <p>6.</p>
              <a href="https://samar-two.vercel.app/">Samar (HTML/CSS/...)</a>
            </li>
            <li>...</li>
          </ol>
        </div>
        {/* Link source code */}
        <div className="basis-1/2 mt-4 pl-20">
          <ol>
            <li>
              <p>1.</p>
              <a href="https://gitlab.com/trannhathao/taixiu_game.git">
                Source code
              </a>
            </li>
            <li>
              <p>2.</p>
              <a href="https://gitlab.com/trannhathao1305/group06_capstone_reactjs.git">
                Source code
              </a>
            </li>
            <li>
              <p>3.</p>
              <a href="https://gitlab.com/trannhathao1305/group06_capstone_airbnbb.git">
                Source code
              </a>
            </li>
            <li>
              <p>4.</p>
              <a href="https://gitlab.com/trannhathao1305/trannhathao_bc35_baitap_reactjs_buoi02_state.git">
                Source code
              </a>
            </li>
            <li>
              <p>5.</p>
              <a href="https://gitlab.com/trannhathao1305/part01-deploy.git">
                Source code
              </a>
            </li>
            <li>
              <p>6.</p>
              <a href="https://gitlab.com/trannhathao/samar.git">Source code</a>
            </li>
            <li>...</li>
          </ol>
        </div>
      </div>
    </div>
  );
}
