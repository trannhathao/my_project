import "./App.css";
import MyProject from "./MyProject/MyProject";

function App() {
  return (
    <div>
      <MyProject />
    </div>
  );
}

export default App;
