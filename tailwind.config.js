/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {},
    listStyleType: {
      none: "none",
      disc: "disc",
      decimal: "decimal",
    },
  },
  plugins: [],
};
